# Documentation #
Our documentation can be found at [http://judge-chimpanzee-58374.bitballoon.com/](http://judge-chimpanzee-58374.bitballoon.com/).

# Requirements #
To compile this project, you'll need the following
```
1. Windows machine
2. Visual Studio 2015 with C++ compilers
3. Git client (optional)
```

# How to compile (NMAKE) #
1. Run "Developer Command Prompt for VS2015" or compatible
2. Navigate to project directory (ie. "cd C:\Projects\stupidcalc-pro\")
3. Run NMAKE "nmake all" to compile the project.
4. YOU IS WIN, CONGLATURATION!

# How to clean up (NMAKE) #
In developer command prompt type "nmake clean". This will clean up your compiled files.

# How to compile (Noob method) #
Double-click on "./stupidcalc-pro/src/FinalProject.sln" and wait until Visual Studio starts up. This might take a while. Now head over to Build > Build Solution in the top menu (or hit Ctrl + Shift + B). The 2 projects should compile without any errors and you are ready to run the program.

# How to run #
Open up command prompt (hit Win + R, and type in "cmd"). Navigate to the project directory (ie. "C:\Projects\stupidcalc-pro\") and find the compiled executables (if you can't find any, you should read the previous sections). If you do find MainExe.exe however, type in "MainExe.exe" in the command prompt. The program should run fine and display a help message to guide you further.