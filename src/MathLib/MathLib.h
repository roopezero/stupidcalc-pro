/**
* @file
* @author Samu H�rk�nen (harkovichs@gmail.com)
* @author Janne Tyni (janne.tyni@hotmail.com)
* @author Roope Huttu (roopezero@gmail.com)
*/

#pragma once

#ifdef MATHLIB_EXPORTS  
#define MATHLIB_API __declspec(dllexport)   
#else  
#define MATHLIB_API __declspec(dllimport)   
#endif

namespace MathLib
{
	class Functions
	{
		public:
			static MATHLIB_API double Pythagoras(double a, double b, double c);
			static MATHLIB_API int Factorial(int n);
			static MATHLIB_API double RectangleArea(double s1, double s2);
			static MATHLIB_API double SquareArea(double s);
			static MATHLIB_API double TriangleArea(double h, double b);
			static MATHLIB_API double CircleArea(double r);
	};
}