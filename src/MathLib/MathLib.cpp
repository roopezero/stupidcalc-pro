/**
* @file
* @author Samu H�rk�nen (harkovichs@gmail.com)
* @author Janne Tyni (janne.tyni@hotmail.com)
* @author Roope Huttu (roopezero@gmail.com)
*/

#define _USE_MATH_DEFINES

#include "MathLib.h"
#include <iostream>
#include <math.h>

/**
 * Collection of mathematic functions.	
 */
namespace MathLib
{
	/**
	 * Basic pythagoras, returns the variable that is left with no value (or 0).
	 * 
	 * @param a Double value, fill or leave it blank (0) if others are filled.
	 * @param b Double value, fill or leave it blank (0) if others are filled.
	 * @param c Double value, fill or leave it blank (0) if others are filled.
	 * @return Value of the remaining parameter. NAN if cannot calculate.
	 */
	double Functions::Pythagoras(double a = 0.0, double b = 0.0, double c = 0.0) {
		if ((a+b+c) == 0.0) {
			return NAN;
		}

		if (a == 0.0) {
			return sqrt(pow(c, 2) - pow(b, 2));
		} else if (b == 0.0) {
			return sqrt(pow(c, 2) - pow(a, 2));
		} else if (c == 0.0) {
			return sqrt(pow(a, 2) + pow(b, 2));
		} return NAN;
	}

	/**
	 * Calculates factorial for n.
	 *
	 * @param n Integer value. Must be greater than 0.
	 * @return Factorial of n. NAN if cannot calculate.
	 */
	int Functions::Factorial(int n) {	
		if (n <= 0) {
			return NAN;
		}

		int result = 1;

		for (int i = 1; i <= n; i++)
		{
			result *= i;
		}

		return result;
	}

	/**
	 * Returns area for a rectangle.
	 *
	 * @param s1 Double value. Width / height of the first side.
	 * @param s2 Double value. Width / height of the second side.
	 * @return Area of the rectangle. NAN if cannot calculate.
	 */
	double Functions::RectangleArea(double s1, double s2) {
		if (s1 + s2 == 0) {
			return NAN;
		}

		return s1*s2;
	}

	/**
	 * Returns area for a square.
	 *
	 * @param s Double value. Length of a side.
	 * @return Area of the square. NAN if cannot calculate.
	 */
	double Functions::SquareArea(double s) {
		if (s == 0) {
			return NAN;
		}

		return RectangleArea(s, s);
	}

	/**
	 * Returns area for a triangle.
	 *
	 * @param h Double value. Height of the triangle.
	 * @param b Double value. Width of the triangle.
	 * @return Area of the triangle. NAN if cannot calculate.
	 */
	double Functions::TriangleArea(double h, double b) {
		if (h + b == 0) {
			return NAN;
		}

		return RectangleArea(h, b) / 2;
	}

	/**
	 * Returns area for a circle.
	 *
	 * @param r Double value. Radius of the circle.
	 * @return Area of the circle. NAN if cannot calculate.
	 */
	double Functions::CircleArea(double r) {
		if (r == 0) {
			return NAN;
		}
		return M_PI * pow(r, 2);
	}
}
