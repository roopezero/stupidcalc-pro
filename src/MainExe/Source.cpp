/**
 * @file
 * @author Samu H�rk�nen (harkovichs@gmail.com)
 * @author Janne Tyni (janne.tyni@hotmail.com)
 * @author Roope Huttu (roopezero@gmail.com)
 * 
 * @section DESCRIPTION
 * 
 * Simple math program which processes a mathematic function 
 * depending on commandline input. After successful calculation
 * the program tries to input the value as registry key to the
 * machine's registry.
 */

#include "constants.h"

/**
 * Simple math program which processes a mathematic function 
 * depending on commandline input.
 * @argc Integer value. Count of commandline arguments.
 * @argv String vector. Contains a number of commandline arguments.
 * @return Returns 0 on success.
 */
int main(int argc, char *argv[]) {	
	char arg = '0';
	char argcalc = '0';
	if (argc > 1) {
		//Open/Create reg key
		HKEY RK;
		LPCTSTR lpSubKey = TEXT(REGSUBKEY);
		DWORD dwDisposition;
		TCHAR o_result[REGSTRSIZE];
		DWORD o_result_l = REGSTRSIZE;

		DWORD o_str_type;
		TCHAR o_calc[REGSTRSIZE];
		DWORD o_calc_l = REGSTRSIZE;
		TCHAR o_input[REGSTRSIZE];
		DWORD o_input_l = REGSTRSIZE;

		//  Attempt to open registry with full read/write access.
		if (RegCreateKeyEx(HKEY_CURRENT_USER, lpSubKey, 0, NULL, 0, KEY_ALL_ACCESS, NULL, &RK, &dwDisposition) != ERROR_SUCCESS) {
			cerr << "ERROR : Couldn't open registry.";
			return 1;
		}

		// Clean commandline argument(s).
		arg = tolower(argv[1][1]);
		// Handle commandline argumen(s).
		switch (arg) {
		case 'h':
			// Display help message.
			cout << "\n\n- StupidCalc Pro -" << "\n\nList of actions:" << "\n -help = Display this help menu." << "\n -version = Display version number."
				<< "\n -last = Display information of the last ran calculation." << "\n -calc = Display a list of possible calculations and run them."
				<< "\n -delete = Delete software use history." << "\n\n--------------";
			break;
		case 'v':
			// Display version of the software.
			cout << "\n\nStupidCalC Pro - Version: " << VERNUM << endl;
			break;
		case 'l':
			// Display last calculation if such exists in the registry.
			if ((RegQueryValueEx(RK, TEXT("last_result"), 0, &o_str_type, (LPBYTE)&o_result, &o_result_l) == ERROR_SUCCESS)
				&& (RegQueryValueEx(RK, TEXT("last_input"), 0, &o_str_type, (LPBYTE)&o_input, &o_input_l) == ERROR_SUCCESS)
				&& (RegQueryValueEx(RK, TEXT("last_calc"), 0, &o_str_type, (LPBYTE)&o_calc, &o_calc_l) == ERROR_SUCCESS)) {		
				wcout << "\n- StupidCalc Pro -" << "\n\nLast used calculation: " << "\n Type: " << o_calc << "\n Variables: " << o_input << "\n Result: " << o_result << "\n\n--------------";
			} else {
				cerr << "No last use history found.";
			}
			break;
		case 'd':
			// Delete history information if such exists in the registry.
			if ((RegDeleteValue(RK, TEXT("last_result")) == ERROR_SUCCESS) &&
				(RegDeleteValue(RK, TEXT("last_input")) == ERROR_SUCCESS) &&
				(RegDeleteValue(RK, TEXT("last_calc")) == ERROR_SUCCESS)) {
				cout << "\nStupidCalc Pro use history succesfully deleted!\n";
			} else {
				cerr << "No last use history found.";
			}
			break;
		case 'c':
			// Process a calculation.
			if (argc > 2) {
				double p1 = NAN, p2 = NAN, p3 = NAN;
				double result = 0;
				wstring calcsel;
				wstring calcinput;
				argcalc = tolower(argv[2][0]);
				// Handle commantline arguments for choosing a mathematic function.
				switch (argcalc) {
				case 'f':
					// Factorial
					if (argv[3] != NULL) {
						sscanf(argv[3], "%lf", &p1);
						result = MathLib::Functions::Factorial( (int)p1 );
						if (isnan(result)) break;
						cout << "Factorial of "<< p1 << " = " << result;
						calcsel = TEXT("Factorial");
					}
					else cout << "No numbers given to calculate, try -help.";
					break;
				case 'p':
					// Pythagoras
					if ((argv[3] != NULL) && (argv[4] != NULL) && (argv[5] != NULL)) {
						sscanf(argv[3], "%lf", &p1);
						sscanf(argv[4], "%lf", &p2);
						sscanf(argv[5], "%lf", &p3);
						result = MathLib::Functions::Pythagoras( p1,p2,p3 );
						if (isnan(result)) break;
						cout << "Pythagoras of " << p1 << ", " << p2 << ", " << p3 << " = " << result;
						calcsel = TEXT("Pythagoras");
					}
					else cout << "No numbers given to calculate, try -help.";
					break;
				case 'r':
					// Area of a rectangle
					if ((argv[3] != NULL) && (argv[4] != NULL)) {
						sscanf(argv[3], "%lf", &p1);
						sscanf(argv[4], "%lf", &p2);
						result = MathLib::Functions::RectangleArea( p1,p2 );
						if (isnan(result)) break;
						cout << "Rectangular area of " << p1 << ", " << p2 << " = " << result;
						calcsel = TEXT("Rectangle");
					}
					else cout << "No numbers given to calculate, try -help.";
					break;
				case 's':
					// Area of a square
					if ((argv[3] != NULL)) {
						sscanf(argv[3], "%lf", &p1);
						result = MathLib::Functions::SquareArea( p1 );
						if (isnan(result)) break;
						cout << "Square area of " << p1 << " = " << result;
						calcsel = TEXT("Square");
					}
					else cout << "No numbers given to calculate, try -help.";
					break;
				case 't':
					// Area of a triangle
					if ((argv[3] != NULL) && (argv[4] != NULL)) {
						sscanf(argv[3], "%lf", &p1);
						sscanf(argv[4], "%lf", &p2);
						result = MathLib::Functions::TriangleArea( p1,p2 );
						if (isnan(result)) break;
						cout << "Triangular area of " << p1 << ", " << p2 << " = " << result;
						calcsel = TEXT("Triangle");
					}
					else cout << "No numbers given to calculate, try -help.";
					break;
				case 'c':
					// Area of a circle
					if ((argv[3] != NULL)) {
						sscanf(argv[3], "%lf", &p1);
						result = MathLib::Functions::CircleArea( p1 );
						if (isnan(result)) break;
						cout << "Circular area of " << p1 << " = " << result;
						calcsel = TEXT("Circle");
					}
					else cout << "No numbers given to calculate, try -help.";
					break;
				}

				// If there is a result, attempt to write a value into the registry.
				if (!isnan(result))	{
					if (!isnan(p1)) calcinput = to_wstring(p1);
					if (!isnan(p2)) calcinput += TEXT(", ") + to_wstring(p2);
					if (!isnan(p3)) calcinput += TEXT(", ") + to_wstring(p3);

					if ((RegSetValueEx(RK, TEXT("last_calc"), 0, REG_SZ, (LPBYTE)calcsel.c_str(), _tcslen(calcsel.c_str())*sizeof(TCHAR)) == ERROR_SUCCESS) &&
						(RegSetValueEx(RK, TEXT("last_input"), 0, REG_SZ, (LPBYTE)calcinput.c_str(), _tcslen(calcinput.c_str())*sizeof(TCHAR)) == ERROR_SUCCESS) &&
						(RegSetValueEx(RK, TEXT("last_result"), 0, REG_SZ, (LPBYTE)to_wstring(result).c_str(), _tcslen(to_wstring(result).c_str())*sizeof(TCHAR)) == ERROR_SUCCESS)) {
						// SUCCESS...
					} else cerr << "Registry saving failed.";
				}
				else cerr << "Incorrect input parameters. Use -calc for more info.";
			}
			else {
				// Display help message for possible calculations.
				cout << "\n\nList of possible calculations:" <<
					"\n fact X = Factorial calculation for whole number (X)" <<
					"\n pyth A B C = Pythagoras, solves value left 0 with remaining values." <<
					"\n rect H W = Area of a rectangle with (H) height and (W) width." <<
					"\n squa W = Area of a square with (W) width." <<
					"\n tria H B = Area of a triangle, with (H) height and (B) basewidth." <<
					"\n circ R = Area of a circle, with (R) radius.\n\n--------------";
			}
			break;
		default:
			break;
		}

		// Close registry.
		RegCloseKey(RK);
	}
	else {
		// Display help message.
		cout << "\n\nTry -help to see a list of possible actions.\n\n";
	}
	return 0;
}

