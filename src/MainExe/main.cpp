#include "MathLib.h"
#include <iostream>


int main(int argc, char* argv[]) {

	std::cout << "factorial: " << MathLib::Functions::Factorial(3) << std::endl;
	std::cout << "pythagoras: " << MathLib::Functions::Pythagoras(0.0, 3.3, 6.5) << std::endl;
	std::cout << "rect area: " << MathLib::Functions::RectangleArea(2.3, 5.6) << std::endl;
	std::cout << "square area: " << MathLib::Functions::SquareArea(3.3) << std::endl;
	std::cout << "triangle area: " << MathLib::Functions::TriangleArea(2.3, 5.6) << std::endl;
	std::cout << "circle area: " << MathLib::Functions::CircleArea(4.5) << std::endl;

	std::cout << std::endl << "HELPERS:" << std::endl;

	std::cout << "factorial: " << MathLib::Functions::Factorial(0) << std::endl;
	std::cout << "pythagoras: " << MathLib::Functions::Pythagoras(0.0, 0.0, 0.0) << std::endl;
	std::cout << "rect area: " << MathLib::Functions::RectangleArea(0.0, 0.0) << std::endl;
	std::cout << "square area: " << MathLib::Functions::SquareArea(0) << std::endl;
	std::cout << "triangle area: " << MathLib::Functions::TriangleArea(0, 0) << std::endl;
	std::cout << "circle area: " << MathLib::Functions::CircleArea(0) << std::endl;

	std::getchar();
	return 0;
}