/**
 * @file
 * @author Samu H�rk�nen (harkovichs@gmail.com)
 * @author Janne Tyni (janne.tyni@hotmail.com)
 * @author Roope Huttu (roopezero@gmail.com)
 */

#include <Windows.h>
#include <ctype.h>
#include <tchar.h>
#include <string>
#include <iostream>
#include "MathLib.h"

using namespace std;

#define VERNUM "1.001"
#define REGSUBKEY "StupidCalc Pro"
#define REGSTRSIZE 1024