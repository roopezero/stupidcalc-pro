# Specify compiler and linker.
CC = cl.exe
LINK = link.exe 

# Build all
all : MathLib.dll MainExe.o MainExe.exe

MathLib.dll : 
	$(CC) "src/MathLib/MathLib.cpp" /link /DLL /out:MathLib.dll

MainExe.exe : 
	$(LINK) /out:MainExe.exe MainExe.o MathLib.lib

MainExe.o : 
	$(CC) /c "src/MainExe/Source.cpp" /FoMainExe.o -I"src/MathLib/" -DUNICODE -D_UNICODE

# Clean up
clean : 
	del MathLib.dll MathLib.exp MathLib.lib MathLib.obj MainExe.exe MainExe.o